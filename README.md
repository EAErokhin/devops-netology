# devops-netology

файл .gitignore указывает, что следующие файлы и папки должны быть проигнорированы:

Все локальные директории .terraform
Файлы .tfstate и .tfstate.*
Файлы crash.log и crash.*.log
Все файлы .tfvars и .tfvars.json
Файлы override.tf, override.tf.json, *_override.tf и *_override.tf.json
Файлы terraformrc и terraform.rc.